package com.example.first;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.first.user.Personfun;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AddActivity extends AppCompatActivity {

    private EditText editText;//正文
    private EditText edittitle;//标题
    private String personid;//id
    private String mNoteTitle;
    private Radio_text radio_text;
    private Title title1;
    private int saveflag;
    private android.view.inputmethod.InputMethodManager inputManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        personid = getIntent().getStringExtra("personid");//接收参数

        //将系统自带的标题栏隐藏掉
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.hide();
        }

        //关联布局控件
        editText = findViewById(R.id.add_text);
        edittitle = findViewById(R.id.add_name);
        radio_text = findViewById(R.id.radioGroup1);
        TextView textView = findViewById(R.id.add_time);
        title1 = findViewById(R.id.title1);

        radio_text.Radio_text1(this);
        title1.Title1(this);
        saveflag = 0;
        textcheck();

        textView.setText(timeStamp1() + " " + timeStamp2() + " ");//将时间填充到

        editText.setFocusable(true);
        editText.setFocusableInTouchMode(true);
        edittitle.setFocusable(true);
        edittitle.setFocusableInTouchMode(true);
        edittitle.clearFocus();
        editText.clearFocus();

        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b) {
                    title1.setcheckINVI();
                    inputManager.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
                } else {
                    title1.setcheckVI();
                    java.util.Timer timer = new java.util.Timer();
                    timer.schedule(new java.util.TimerTask() {
                        public void run() {
                            inputManager = (android.view.inputmethod.InputMethodManager) editText.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                            assert inputManager != null;
                            inputManager.showSoftInput(editText, 0);
                        }
                    }, 300);
                }
            }
        });

        edittitle.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b) {
                    title1.setcheckINVI();
                    inputManager.hideSoftInputFromInputMethod(view.getWindowToken(), 0);
                } else {
                    title1.setcheckVI();
                    java.util.Timer timer = new java.util.Timer();
                    timer.schedule(new java.util.TimerTask() {
                        public void run() {
                            inputManager = (android.view.inputmethod.InputMethodManager) editText.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                            assert inputManager != null;
                            inputManager.showSoftInput(edittitle, 0);
                        }
                    }, 300);
                }
            }
        });

        editText.addTextChangedListener(textWatcher);
        edittitle.addTextChangedListener(nameWatcher);
    }

    private TextWatcher textWatcher = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            textcheck();
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    private TextWatcher nameWatcher = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            textcheck();
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    public void textcheck() {
        String inputText = editText.getText().toString();//将获取到的EditText内的文本转化为字符串
        String inputtitle = edittitle.getText().toString();//将获取到的EditText内的文本转化为字符串
        if (TextUtils.isEmpty(inputText) && TextUtils.isEmpty(inputtitle)) {
            title1.setcheckdown();
        } else {
            title1.setcheckup();
        }
    }


    @Override//销毁页面前保存文本
    protected void onDestroy() {
        super.onDestroy();
        saveadd();
    }


    //时间戳
    public static String timeStamp1() {
        Date time = new Date();
        SimpleDateFormat format1 = new SimpleDateFormat("yy-MM-dd");//定义时间格式
        return format1.format(time);
    }

    public static String timeStamp2() {
        Date time = new Date();
        SimpleDateFormat format2 = new SimpleDateFormat("HH:mm:ss");//定义时间格式
        return format2.format(time);
    }


    //存储时间和前十位文本
    private void save(String text, String title) {
        BufferedWriter writertext = null;
        String saveFileName;
        String saveFileday;
        String saveFiletime;
        saveFileday = timeStamp1();
        saveFiletime = timeStamp2();

        if (title.length() > 0) {
            saveFileName = title;
        } else {
            if (text.length() >= 9) {
                saveFileName = text.substring(0, 9);
            } else {
                saveFileName = text;
            }
        }

        if (saveflag == 0) {
            try {
                FileOutputStream outtext = openFileOutput(saveFileName, Context.MODE_PRIVATE);
                writertext = new BufferedWriter(new OutputStreamWriter(outtext));
                writertext.write(text);
                Personfun personfun = new Personfun(AddActivity.this);
                boolean flag;
                flag = personfun.addbook(saveFileName, saveFileday, saveFiletime);
                if (flag) {
                    int bookid = personfun.findbookid(saveFileName);
                    mNoteTitle = saveFileName;
                    flag = personfun.addperson(bookid, personid);
                    if (!flag) {
                        Log.i("TAG", "添加归属失败");
                        Toast.makeText(AddActivity.this, "添加归属失败", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Log.i("TAG", "添加记录失败");
                    Toast.makeText(AddActivity.this, "添加记录失败", Toast.LENGTH_LONG).show();
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (writertext != null) {
                        writertext.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            try {
                FileOutputStream outtext = openFileOutput(saveFileName, Context.MODE_PRIVATE);
                writertext = new BufferedWriter(new OutputStreamWriter(outtext));
                writertext.write(text);
                Personfun personfun = new Personfun(AddActivity.this);
                int bookid = personfun.findbookid(mNoteTitle);
                if (!mNoteTitle.equals(saveFileName)) {
                    boolean flag;
                    mNoteTitle = saveFileName;
                    flag = personfun.changebook(saveFileName, bookid);
                    if (!flag) {
                        Log.i("TAG", "修改标题失败");
                        Toast.makeText(AddActivity.this, "修改标题失败", Toast.LENGTH_LONG).show();
                    } else {
                        flag = personfun.changenewday(saveFileday, bookid);
                        if (!flag) {
                            Log.i("TAG", "修改日期失败");
                            Toast.makeText(AddActivity.this, "修改日期失败", Toast.LENGTH_LONG).show();
                        } else {
                            flag = personfun.changenewtime(saveFiletime, bookid);
                            if (!flag) {
                                Log.i("TAG", "修改时间失败");
                                Toast.makeText(AddActivity.this, "修改时间失败", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (writertext != null) {
                        writertext.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    //分享方法
    public void shareMsg() {
        editText.getText(); //获取文本
        String sharetext = editText.getText().toString();//转化为字符串

        if (!sharetext.equals("")) {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain"); // 纯文本

            intent.putExtra(Intent.EXTRA_TEXT, sharetext);//分享内容
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(Intent.createChooser(intent, "分享文本"));//弹框标题
        } else if (sharetext.equals("")) {
            Toast.makeText(getApplicationContext(), "没有内容", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getApplicationContext(), "分享失败", Toast.LENGTH_LONG).show();
        }
    }

    @SuppressLint("SdCardPath")
    public void saveadd() {
        if (saveflag == 0) {
            String inputText = editText.getText().toString();//将获取到的EditText内的文本转化为字符串
            String inputtitle = edittitle.getText().toString();//将获取到的EditText内的文本转化为字符串
            save(inputText, inputtitle);//存储到文件指定文件中
            editText.clearFocus();
            edittitle.clearFocus();
            saveflag = 1;
        } else {
            FileInputStream in;
            BufferedReader reader = null;
            StringBuilder content = new StringBuilder();
            try {
                in = openFileInput(mNoteTitle);//从默认目录加载data文件，返回一个FileInputStream对象
                reader = new BufferedReader(new InputStreamReader(in));//读取对象
                String line;
                while ((line = reader.readLine()) != null) {//读取一行文本
                    content.append(line).append("\n");//每次都在末尾插入文本
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            String loadText1 = content.toString();//将content转化成字符串inputText
            String loadText = null;
            if (loadText1.length() != 0) {
                loadText = loadText1.substring(0, loadText1.length() - 1);//去掉最末尾的\n
            }

            //获取当前输入的内容
            String inputText = editText.getText().toString();//将获取到的EditText内的文本转化为字符串
            String inputtitle = edittitle.getText().toString();
            if (!TextUtils.isEmpty(inputText)) {
                if (!inputText.equals(loadText) || !mNoteTitle.equals(inputtitle)) {//和之前不一样
                    save(inputText, inputtitle);
                    editText.clearFocus();
                    edittitle.clearFocus();
                }
                if (!mNoteTitle.equals(inputtitle)) {
                    deleteFileName("/data/data/com.example.first/files/" + mNoteTitle);
                }
            } else if (TextUtils.isEmpty(inputText) && !TextUtils.isEmpty(inputtitle)) {
                if (!mNoteTitle.equals(inputtitle)) {//和之前不一样
                    save(inputText, inputtitle);
                    editText.clearFocus();
                    edittitle.clearFocus();
                    deleteFileName("/data/data/com.example.first/files/" + mNoteTitle);
                }
            } else if (TextUtils.isEmpty(inputText) && TextUtils.isEmpty(inputtitle)) {
                deleteFileName("/data/data/com.example.first/files/" + mNoteTitle);
                deletebook(mNoteTitle);
            }
        }
    }

    public void deleteFileName(String fileName) {
        File file = new File(fileName);
        // 如果文件路径所对应的文件存在，并且是一个文件，则直接删除
        if (file.exists() && file.isFile()) {
            if (!file.delete()) {
                Toast.makeText(getApplicationContext(), "删除失败", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getApplicationContext(), "文件" + fileName + "不存在", Toast.LENGTH_SHORT).show();
        }
    }

    public void deletebook(String name) {
        Personfun personfun = new Personfun(AddActivity.this);
        int bookid = personfun.findbookid(name);
        personfun.deperson(bookid);
        personfun.debook(bookid);
    }

    public void radiodelete1() {
        if (saveflag == 0) {
            Toast.makeText(getApplicationContext(), "请先保存记录", Toast.LENGTH_SHORT).show();
        } else {
            AlertDialog.Builder dialog = new AlertDialog.Builder(AddActivity.this);
            dialog.setTitle("提示");
            dialog.setMessage("是否删除该记录?");
            dialog.setPositiveButton("删除", new DialogInterface.OnClickListener() {
                @SuppressLint("SdCardPath")
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String nulltext = null;
                    String nulltitle = null;
                    editText.setText(nulltext);
                    edittitle.setText(nulltitle);
                    finish();
                }
            });
            dialog.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            dialog.show();
        }
    }

}