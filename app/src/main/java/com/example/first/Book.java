package com.example.first;

public class Book {
    private String name;
    private String newday;
    private String newtime;

    Book(String name, String newday, String newtime) {
        this.name = name;
        this.newday = newday;
        this.newtime = newtime;
    }

    public String getNewday() {
        return newday;
    }

    public String getName() {
        return name;
    }

    public String getNewtime() {
        return newtime;
    }

}
