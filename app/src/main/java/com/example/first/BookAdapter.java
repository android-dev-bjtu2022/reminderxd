package com.example.first;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.first.user.Personfun;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.ViewHolder> {
    private List<Book> mBookList;
    private List<Book> backList;
    private MyFilter mFilter;
    private MainActivity mainActivity;//引用


    static class ViewHolder extends RecyclerView.ViewHolder {//定义内部类：ViewHolder，继承自RecyclerView.ViewHolder
        TextView bookName;
        TextView bookday;
        View bookView;//点击

        ViewHolder(View view) {//ViewHolder的构造函数中传入参数view
            super(view);
            bookName = view.findViewById(R.id.book_name);
            bookday = view.findViewById(R.id.book_day);
            bookView = view;//点击

        }
    }

    BookAdapter(MainActivity mainActivity, List<Book> bookList) {//将展示的数据源传给bookList
        this.mainActivity = mainActivity;
        mBookList = bookList;//赋值给一个全局变量mBookList，后续操作都在mBookList数据源进行
        backList = bookList;
    }

    //因为BookAdapter是继承自RecyclerView.Adapter的，所以必须重写onCreateViewHolder、onBindViewHolder、getItemCount三个方法

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);//加载自定义item布局
        final ViewHolder holder = new ViewHolder(view);//创建一个ViewHolder实例，把加载的布局传到构造函数中
        //点击事件监听
        holder.bookView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = holder.getAdapterPosition();//获取用户点击的position
                Book book = mBookList.get(position);//通过position拿到Book实例
                Intent Edit = new Intent(mainActivity, EditActivity.class);
                Edit.putExtra("title", book.getName());//获取文件名字，传递参数

                mainActivity.startActivity(Edit);

            }
        });
        //长按事件监听
        holder.bookView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                int position = holder.getAdapterPosition();
                final Book book = mBookList.get(position);

                //提示框
                AlertDialog.Builder dialog = new AlertDialog.Builder(mainActivity);

                //去掉换行
                String getName1 = null;
                if (book.getName() != null) {
                    Pattern p = Pattern.compile("[*\t\r\n]");
                    Matcher m = p.matcher(book.getName());
                    getName1 = m.replaceAll("");
                }

                dialog.setMessage("删除: " + getName1 + " ?");
                dialog.setPositiveButton("确认", new DialogInterface.OnClickListener() {
                    @SuppressLint("SdCardPath")
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteFileName("/data/data/com.example.first/files/" + book.getName());//删除文件
                        mainActivity.deletebook(book.getName());
                        mainActivity.notifyData();//刷新列表
                    }
                });
                dialog.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mainActivity.notifyData();//刷新列表
                    }
                });
                dialog.show();

                return false;
            }
        });
        return holder;//将holder实例返回
    }

    public Filter getFilter() {
        if (mFilter == null) {
            mFilter = new MyFilter();
        }
        return mFilter;
    }

    class MyFilter extends Filter {
        @Override
        //定义过滤规则
        protected FilterResults performFiltering(CharSequence charSequence) {
            FilterResults result = new FilterResults();
            List<Book> list;
            if (TextUtils.isEmpty(charSequence)) {//当过滤的关键字为空的时候
                list = null;
            } else {//把符合条件的数据对象添加到集合中
                list = new ArrayList<>();
                for (Book book : backList) {
                    if (book.getName().contains(charSequence)) {
                        list.add(book);
                    }
                }
            }
            result.values = list; //将得到的集合保存到FilterResults的value变量中
            result.count = list.size();//将集合的大小保存到FilterResults的count变量中

            mainActivity.setBookList(list);
            return result;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            mBookList = (List<Book>) filterResults.values;
            if (filterResults.count > 0) {
                notifyDataSetChanged();//通知数据发生了改变
            } else {
            }
        }
    }

    @Override//对RecyclerView子项数据赋值，会在每个子项被滚动到屏幕内的时候执行
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Book book = mBookList.get(position);//通过position参数得到当前项的Book实例
        holder.bookday.setText(book.getNewday());
        holder.bookName.setText(book.getName());//将数据设置到ViewHolder的bookName中
    }

    @Override//告诉RecyclerView一共有多少个子项
    public int getItemCount() {
        return mBookList.size();//返回数据源长度
    }

    //删除单个文件
    private void deleteFileName(String fileName) {
        File file = new File(fileName);
        // 如果文件路径所对应的文件存在，并且是一个文件，则直接删除
        if (file.exists() && file.isFile()) {
            if (!file.delete()) {
                Toast.makeText(mainActivity, "删除失败", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(mainActivity, "文件" + fileName + "不存在", Toast.LENGTH_SHORT).show();
        }
    }
}
