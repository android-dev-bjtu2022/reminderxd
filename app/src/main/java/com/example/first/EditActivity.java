package com.example.first;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import static com.example.first.AddActivity.timeStamp1;
import static com.example.first.AddActivity.timeStamp2;

import com.example.first.user.Personfun;

public class EditActivity extends AppCompatActivity {

    private EditText editText;//正文
    private EditText editname;//正文
    private String mNoteTitle;//文件名（标题）
    private Radio_text radio_text;
    private Title title2;
    private android.view.inputmethod.InputMethodManager inputManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        //将系统自带的标题栏隐藏掉
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.hide();
        }

        //关联布局控件
        editText = findViewById(R.id.edit_text);
        editname = findViewById(R.id.edit_name);
        radio_text = findViewById(R.id.radioGroup2);
        TextView textView = findViewById(R.id.edit_time);
        title2 = findViewById(R.id.title2);

        radio_text.Radio_text2(this);
        title2.Title2(this);

        mNoteTitle = getIntent().getStringExtra("title");//接收参数
        editname.setText(mNoteTitle);
        String loadtext = load(mNoteTitle);//读取
        editText.setText(loadtext);//将inputText内容填充到editText
        Personfun personfun = new Personfun(EditActivity.this);
        String newday = personfun.findnewday(mNoteTitle);
        textView.setText(newday);//将时间填充到

        editText.setFocusable(true);
        editText.setFocusableInTouchMode(true);
        editname.setFocusable(true);
        editname.setFocusableInTouchMode(true);
        editText.clearFocus();
        editname.clearFocus();
        title2.setcheckINVI();

        editText.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b) {
                    title2.setcheckINVI();
                    inputManager.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
                } else {
                    title2.setcheckVI();
                    java.util.Timer timer = new java.util.Timer();
                    timer.schedule(new java.util.TimerTask() {
                        public void run() {
                            inputManager = (android.view.inputmethod.InputMethodManager) editText.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                            assert inputManager != null;
                            inputManager.showSoftInput(editText, 0);
                        }
                    }, 300);
                }
            }
        });

        editname.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b) {
                    title2.setcheckINVI();
                    inputManager.hideSoftInputFromInputMethod(view.getWindowToken(), 0);
                } else {
                    title2.setcheckVI();
                    java.util.Timer timer = new java.util.Timer();
                    timer.schedule(new java.util.TimerTask() {
                        public void run() {
                            inputManager = (android.view.inputmethod.InputMethodManager) editText.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                            assert inputManager != null;
                            inputManager.showSoftInput(editname, 0);
                        }
                    }, 300);
                }
            }
        });

        editText.addTextChangedListener(textWatcher);
        editname.addTextChangedListener(nameWatcher);

    }

    private TextWatcher textWatcher = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            textcheck();
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    private TextWatcher nameWatcher = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            textcheck();
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    @SuppressLint("SdCardPath")
    @Override//销毁页面
    protected void onDestroy() {

        super.onDestroy();
    }

    public void asksave(String newtext, String newtitle) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(EditActivity.this);
        dialog.setTitle("提示");
        dialog.setMessage("是否保存更改?");
        dialog.setPositiveButton("保存", new DialogInterface.OnClickListener() {
            @SuppressLint("SdCardPath")
            @Override
            public void onClick(DialogInterface dialog, int which) {
                save(newtext, newtitle);//存储到文件指定文件中
                finish();
            }
        });
        dialog.setNegativeButton("不保存", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        dialog.show();
    }

    public void askde() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(EditActivity.this);
        dialog.setTitle("提示");
        dialog.setMessage("是否保存更改?");
        dialog.setPositiveButton("保存", new DialogInterface.OnClickListener() {
            @SuppressLint("SdCardPath")
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteFileName("/data/data/com.example.first/files/" + mNoteTitle);
                deletebook(mNoteTitle);
                finish();
            }
        });
        dialog.setNegativeButton("不保存", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        dialog.show();
    }


    //读取
    public String load(String name) {
        FileInputStream in;
        BufferedReader reader = null;
        StringBuilder content = new StringBuilder();
        try {
            in = openFileInput(name);//从默认目录加载data文件，返回一个FileInputStream对象
            reader = new BufferedReader(new InputStreamReader(in));//读取对象
            String line;
            while ((line = reader.readLine()) != null) {//读取一行文本
                content.append(line).append("\n");//每次都在末尾插入文本,插入换行
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        String loadText1 = content.toString();//将content转化成字符串inputText
        String loadText = null;
        if (loadText1.length() != 0) {
            loadText = loadText1.substring(0, loadText1.length() - 1);
        }

        return loadText;
    }


    //存储标题、文本
    private void save(String text, String title) {
        BufferedWriter writertext = null;
        String saveFileName;
        String saveFileday;
        String saveFiletime;
        saveFileday = timeStamp1();
        saveFiletime = timeStamp2();

        if (title.length() > 0) {
            saveFileName = title;
        } else {
            if (text.length() >= 9) {
                saveFileName = text.substring(0, 9);
            } else {
                saveFileName = text;
            }
        }

        try {
            FileOutputStream outtext = openFileOutput(saveFileName, Context.MODE_PRIVATE);
            writertext = new BufferedWriter(new OutputStreamWriter(outtext));
            writertext.write(text);
            Personfun personfun = new Personfun(EditActivity.this);
            int bookid = personfun.findbookid(mNoteTitle);
            if (!mNoteTitle.equals(saveFileName)) {
                boolean flag;
                mNoteTitle = saveFileName;
                flag = personfun.changebook(saveFileName, bookid);
                if (!flag) {
                    Log.i("TAG", "修改标题失败");
                    Toast.makeText(EditActivity.this, "修改标题失败", Toast.LENGTH_LONG).show();
                } else {
                    flag = personfun.changenewday(saveFileday, bookid);
                    if (!flag) {
                        Log.i("TAG", "修改日期失败");
                        Toast.makeText(EditActivity.this, "修改日期失败", Toast.LENGTH_LONG).show();
                    } else {
                        flag = personfun.changenewtime(saveFiletime, bookid);
                        if (!flag) {
                            Log.i("TAG", "修改时间失败");
                            Toast.makeText(EditActivity.this, "修改时间失败", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            } else {
                boolean flag;
                flag = personfun.changenewday(saveFileday, bookid);
                if (!flag) {
                    Log.i("TAG", "修改日期失败");
                    Toast.makeText(EditActivity.this, "修改日期失败", Toast.LENGTH_LONG).show();
                } else {
                    flag = personfun.changenewtime(saveFiletime, bookid);
                    if (!flag) {
                        Log.i("TAG", "修改时间失败");
                        Toast.makeText(EditActivity.this, "修改时间失败", Toast.LENGTH_LONG).show();
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (writertext != null) {
                    writertext.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //删除单个文件
    public void deleteFileName(String fileName) {
        File file = new File(fileName);
        // 如果文件路径所对应的文件存在，并且是一个文件，则直接删除
        if (file.exists() && file.isFile()) {
            if (!file.delete()) {
                Toast.makeText(getApplicationContext(), "删除失败", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getApplicationContext(), "文件" + fileName + "不存在", Toast.LENGTH_SHORT).show();
        }
    }

    //分享方法
    public void shareMsg() {
        editText.getText(); //获取文本
        String text = editText.getText().toString();//转化为字符串

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain"); // 纯文本

        intent.putExtra(Intent.EXTRA_TEXT, text);//分享内容
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(Intent.createChooser(intent, "分享文本"));//弹框标题
    }

    public void deletebook(String name) {
        Personfun personfun = new Personfun(EditActivity.this);
        int bookid = personfun.findbookid(name);
        personfun.deperson(bookid);
        personfun.debook(bookid);

    }

    @SuppressLint("SdCardPath")
    public void radiodelete() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(EditActivity.this);
        dialog.setTitle("提示");
        dialog.setMessage("是否删除该记录?");
        dialog.setPositiveButton("删除", new DialogInterface.OnClickListener() {
            @SuppressLint("SdCardPath")
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteFileName("/data/data/com.example.first/files/" + mNoteTitle);
                deletebook(mNoteTitle);
                finish();
            }
        });
        dialog.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        dialog.show();
    }

    public void textcheck() {
        String inputText = editText.getText().toString();//将获取到的EditText内的文本转化为字符串
        String inputtitle = editname.getText().toString();//将获取到的EditText内的文本转化为字符串
        if (TextUtils.isEmpty(inputText) && TextUtils.isEmpty(inputtitle)) {
            title2.setcheckdown();
        } else {
            title2.setcheckup();
        }
    }

    @SuppressLint("SdCardPath")
    public void saveedit() {
        String inputText = editText.getText().toString();//将获取到的EditText内的文本转化为字符串
        String inputtitle = editname.getText().toString();//将获取到的EditText内的文本转化为字符串
        String loadtext = load(mNoteTitle);
        if (!TextUtils.isEmpty(inputText)) {
            if (!inputText.equals(loadtext) || !mNoteTitle.equals(inputtitle)) {//和之前不一样
                save(inputText, inputtitle);//存储到文件指定文件中
                mNoteTitle = inputtitle;
                editText.clearFocus();
                editname.clearFocus();
            } else {
                editText.clearFocus();
                editname.clearFocus();
            }
            if (!mNoteTitle.equals(inputtitle)) {
                deleteFileName("/data/data/com.example.first/files/" + mNoteTitle);
            }
        } else if (TextUtils.isEmpty(inputText) && !TextUtils.isEmpty(inputtitle)) {
            if (!mNoteTitle.equals(inputtitle)) {//和之前不一样
                save(inputText, inputtitle);//存储到文件指定文件中
                deleteFileName("/data/data/com.example.first/files/" + mNoteTitle);
                mNoteTitle = inputtitle;
                editText.clearFocus();
                editname.clearFocus();
            } else {
                editText.clearFocus();
                editname.clearFocus();
            }
        }
    }

    @SuppressLint("SdCardPath")
    public void outsave() {
        //读取之前的内容
        FileInputStream in;
        BufferedReader reader = null;
        StringBuilder content = new StringBuilder();
        try {
            in = openFileInput(mNoteTitle);//从默认目录加载data文件，返回一个FileInputStream对象
            reader = new BufferedReader(new InputStreamReader(in));//读取对象
            String line;
            while ((line = reader.readLine()) != null) {//读取一行文本
                content.append(line).append("\n");//每次都在末尾插入文本
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        String loadText1 = content.toString();//将content转化成字符串inputText
        String loadText = null;
        if (loadText1.length() != 0) {
            loadText = loadText1.substring(0, loadText1.length() - 1);//去掉最末尾的\n
        }

        //获取当前输入的内容
        String inputText = editText.getText().toString();//将获取到的EditText内的文本转化为字符串
        String inputtitle = editname.getText().toString();
        if (!TextUtils.isEmpty(inputText)) {
            if (!inputText.equals(loadText) || !mNoteTitle.equals(inputtitle)) {//和之前不一样
                asksave(inputText, inputtitle);
            }
            if (!mNoteTitle.equals(inputtitle)) {
                deleteFileName("/data/data/com.example.first/files/" + mNoteTitle);
            }
        } else if (TextUtils.isEmpty(inputText) && !TextUtils.isEmpty(inputtitle)) {
            if (!mNoteTitle.equals(inputtitle)) {//和之前不一样
                asksave(inputText, inputtitle);
                deleteFileName("/data/data/com.example.first/files/" + mNoteTitle);
            }
        } else if (TextUtils.isEmpty(inputText) && TextUtils.isEmpty(inputtitle)) {
            askde();
        }
    }
}