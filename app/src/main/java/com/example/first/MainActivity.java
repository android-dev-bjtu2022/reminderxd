package com.example.first;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.widget.SearchView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static com.example.first.AddActivity.timeStamp1;
import static com.example.first.AddActivity.timeStamp2;

import com.example.first.user.Personfun;

public class MainActivity extends AppCompatActivity {

    private String mpersonid;//id

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        MenuItem mSearch = menu.findItem(R.id.action_search);
        SearchView mSearchView = (SearchView) mSearch.getActionView();

        //搜索图标是否显示在搜索框内
        mSearchView.setIconifiedByDefault(true);
        //设置搜索框展开时是否显示提交按钮，可不显示
        mSearchView.setSubmitButtonEnabled(true);
        //让键盘的回车键设置成搜索
        mSearchView.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
        //搜索框是否展开，false表示展开
        mSearchView.setIconified(false);
        //获取焦点
        mSearchView.setFocusable(true);
        mSearchView.requestFocusFromTouch();
        //设置提示词
        mSearchView.setQueryHint("请输入关键字");
        BookAdapter adapter = new BookAdapter(this, bookList);

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                notifyData();
                adapter.getFilter().filter(newText);
                return false;
            }

        });


        mSearchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                //TODO 添加关闭事件
                notifyData();
                return false;
            }
        });
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.deletename_item:
                AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);
                dialog.setTitle("提示");
                dialog.setMessage("删除全部记录?");
                dialog.setPositiveButton("确认", new DialogInterface.OnClickListener() {
                    @SuppressLint("SdCardPath")
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteDir("/data/data/com.example.first/files/");//全部删除
                        notifyData();//刷新列表
                    }
                });
                dialog.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        notifyData();
                    }
                });
                dialog.show();
                break;
        }
        return true;
    }


    @Override
    protected void onResume() {
        super.onResume();
        new Handler().postDelayed(new Runnable() {
            @Override//延时
            public void run() {
                notifyData();
            }
        }, 800);
    }

    public void notifyData() {
        initBook(mpersonid);//重新初始化数据。
        RecyclerView recyclerView = findViewById(R.id.recycler_view);//获取到RecyclerView实例
        recyclerView.getAdapter().notifyDataSetChanged();//刷新列表
    }

    public void setBookList(List<Book> List) {
        bookList.clear();
        bookList.addAll(List);
        RecyclerView recyclerView = findViewById(R.id.recycler_view);//获取到RecyclerView实例
        recyclerView.getAdapter().notifyDataSetChanged();//刷新列表
    }


    private List<Book> bookList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mpersonid = getIntent().getStringExtra("personid");//接收参数

        initBook(mpersonid);//初始化Book数据
        RecyclerView recyclerView = findViewById(R.id.recycler_view);//获取到RecyclerView实例
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);//指定布局方式，LinearLayoutManager：线性布局，可以实现和listview类似的效果
        recyclerView.setLayoutManager(layoutManager);//设置布局管理器
        BookAdapter adapter = new BookAdapter(this, bookList);//将bookList数据传入到BookAdapter的构造函数中
        recyclerView.setAdapter(adapter);//设置adapter

        //设置item间距
        recyclerView.addItemDecoration(new SpacesItemDecoration(40));
        recyclerView.setAdapter(adapter);

        //注册add按钮的监听事件
        Button button_add = findViewById(R.id.button_add);
        button_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.button_add:
                        Intent Add = new Intent(MainActivity.this, AddActivity.class);
                        Add.putExtra("personid", mpersonid);//获取id，传递参数
                        startActivity(Add);
                        break;
                }
            }
        });
    }


    private void initBook(String personid) {
        bookList.clear();//清空数据
        Personfun personfun = new Personfun(MainActivity.this);
        String[] str = personfun.findallname(personid);
        for (String s : str) {
            String newday = personfun.findnewday(s);
            String newtime = personfun.findnewtime(s);
            Book a = new Book(s, newday, newtime);
            bookList.add(a);
        }
        Collections.sort(bookList, new Comparator<Book>() {
            @Override
            public int compare(Book o1, Book o2) {
                return o2.getName().compareTo(o1.getName());
            }
        });

    }


    //删除目录下所有文件
    public void deleteDir(final String pPath) {
        File dir = new File(pPath);
        deleteDirWihtFile(dir);
        deleteper(mpersonid);
    }

    public static void deleteDirWihtFile(File dir) {
        if (dir == null || !dir.exists() || !dir.isDirectory())
            return;
        for (File file : dir.listFiles()) {
            if (file.isFile())
                file.delete(); // 删除所有文件
            else if (file.isDirectory())
                deleteDirWihtFile(file); // 递规的方式删除文件夹
        }
        //dir.delete();// 删除目录本身
    }

    public void deleteper(final String personid) {
        Personfun personfun = new Personfun(MainActivity.this);
        int[] bookid = personfun.findallid(personid);
        boolean flag1, flag2;
        for (int x : bookid) {
            personfun.deperson(x);
            personfun.debook(x);
        }
    }

    public void deletebook(String name) {
        Personfun personfun = new Personfun(MainActivity.this);
        int bookid = personfun.findbookid(name);
        personfun.deperson(bookid);
        personfun.debook(bookid);

    }

}