package com.example.first;

public class Person {
    private int bookid;
    private String personid;

    Person(int bookid, String personid) {
        this.bookid = bookid;
        this.personid = personid;
    }

    public int getBookid() {
        return bookid;
    }

    public String getPersonid() {
        return personid;
    }
}
