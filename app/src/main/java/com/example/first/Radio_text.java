package com.example.first;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class Radio_text extends LinearLayout {
    private EditActivity editActivity;
    private AddActivity addActivity;
    private RadioButton shareButton;
    private RadioButton deleteButon;
    private RadioGroup radiogroup;

    public Radio_text(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.radiogroup, this);
        radiogroup = findViewById(R.id.radioGroup);

        radiogroup.clearCheck();
        radiogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                String edit = "EditActivity";
                String add = "AddActivity";
                String name = ((Activity) getContext()).getClass().getSimpleName();
                switch (checkedId) {
                    case R.id.radioButton_share:
                        if (name.equals(edit)) {
                            editActivity.shareMsg();
                        } else if (name.equals(add)) {
                            addActivity.shareMsg();
                        }
                        radiogroup.clearCheck();
                        break;
                    case R.id.radioButton_delete:
                        if (name.equals(edit)) {
                            editActivity.radiodelete();
                        } else if (name.equals(add)) {
                            addActivity.radiodelete1();
                        }
                        radiogroup.clearCheck();
                        break;
                }
            }
        });

    }

    public void Radio_text1(AddActivity addActivity) {
        this.addActivity = addActivity;
    }

    public void Radio_text2(EditActivity editActivity) {
        this.editActivity = editActivity;
    }

}
