package com.example.first;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.first.user.Userfun;

public class RegisterActivity extends AppCompatActivity {

    EditText username;
    EditText password;
    EditText password2;
    Button register;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        findViews();
        register.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String name = username.getText().toString().trim();
                String pass = password.getText().toString().trim();
                String pass2 = password2.getText().toString().trim();
                Log.i("TAG", name + "_" + pass + "_" + pass2);

                Userfun uSerfun = new Userfun(RegisterActivity.this);
                boolean flag = uSerfun.find(name);
                if (flag) {
                    if (pass.equals(pass2)) {
                        User user = new User();
                        user.setUsername(name);
                        user.setPassword(pass);
                        uSerfun.register(user);
                        Log.i("TAG", "注册成功");
                        Toast.makeText(RegisterActivity.this, "注册成功", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                        startActivity(intent);
                    } else {
                        Log.i("TAG", "密码不一致");
                        Toast.makeText(RegisterActivity.this, "两次密码不一致", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Log.i("TAG", "用户名已存在");
                    Toast.makeText(RegisterActivity.this, "用户名已存在", Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    private void findViews() {
        username = (EditText) findViewById(R.id.usernameRegister);
        password = (EditText) findViewById(R.id.passwordRegister);
        password2 = (EditText) findViewById(R.id.password2Register);
        register = (Button) findViewById(R.id.Register);
    }
}