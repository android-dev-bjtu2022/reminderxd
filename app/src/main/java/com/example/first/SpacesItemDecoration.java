package com.example.first;

import android.graphics.Rect;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
    private int space;

    SpacesItemDecoration(int space) {
        this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.left = 20;
        outRect.right = 20;
        outRect.bottom = space;

        //仅为第一个项目添加上边距，以避免项目之间的双倍空间
        if (parent.getChildAdapterPosition(view) == 0)
            outRect.top = space;
    }

}

