package com.example.first;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

public class Title extends LinearLayout {

    private EditActivity editActivity;
    private AddActivity addActivity;
    private Button titleBack;
    private Button titlecheck;

    public Title(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.title, this);
        titleBack = findViewById(R.id.title_back);
        titlecheck = findViewById(R.id.title_check);
        titleBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String edit = "EditActivity";
                String add = "AddActivity";
                String name = ((Activity) getContext()).getClass().getSimpleName();
                if (name.equals(edit)) {
                    editActivity.outsave();
                } else if (name.equals(add)) {
                    ((Activity) getContext()).finish();
                }
            }
        });

        titlecheck.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String edit = "EditActivity";
                String add = "AddActivity";
                String name = ((Activity) getContext()).getClass().getSimpleName();
                if (name.equals(edit)) {
                    editActivity.saveedit();
                } else if (name.equals(add)) {
                    addActivity.saveadd();
                }
            }
        });
    }

    public void Title1(AddActivity addActivity) {
        this.addActivity = addActivity;
    }

    public void Title2(EditActivity editActivity) {
        this.editActivity = editActivity;
    }

    public void setcheckINVI() {
        titlecheck.setVisibility(View.INVISIBLE);
    }

    public void setcheckVI() {
        titlecheck.setVisibility(View.VISIBLE);
    }

    public void setcheckdown() {
        titlecheck.setEnabled(false);
    }

    public void setcheckup() {
        titlecheck.setEnabled(true);
    }

}