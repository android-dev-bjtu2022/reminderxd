package com.example.first.user;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MyDatabaseHelper extends SQLiteOpenHelper {
    static String name = "user.db";
    static int dbVersion = 1;

    public MyDatabaseHelper(Context context) {
        super(context, name, null, dbVersion);
    }

    public void onCreate(SQLiteDatabase db) {
        String sql1 = "create table user(id integer primary key autoincrement,username varchar(20),password varchar(20))";
        db.execSQL(sql1);
        String sql2 = "create table person(bookid integer,personid varchar(20))";
        db.execSQL(sql2);
        String sql3 = "create table Book(bookid integer primary key autoincrement,name varchar(40),newday varchar(20),newtime varchar(20))";
        db.execSQL(sql3);

    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}