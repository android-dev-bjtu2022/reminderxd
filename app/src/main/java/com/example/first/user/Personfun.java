package com.example.first.user;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class Personfun {
    private MyDatabaseHelper dbHelper;

    public Personfun(Context context) {
        dbHelper = new MyDatabaseHelper(context);
    }

    public int[] findallid(String personid) {
        SQLiteDatabase sdb = dbHelper.getReadableDatabase();

        String sql = "select Book.bookid from Book,person where Book.bookid=person.bookid and personid=?";
        Cursor cursor = sdb.rawQuery(sql, new String[]{personid});
        int[] str = new int[cursor.getCount()];
        int i = 0;
        while (cursor.moveToNext()) {
            int id = cursor.getInt(0);
            str[i] = id;
            i++;
        }
        cursor.close();
        return str;
    }

    public String[] findallname(String personid) {
        SQLiteDatabase sdb = dbHelper.getReadableDatabase();

        String sql = "select name from Book,person where Book.bookid=person.bookid and personid=?";
        Cursor cursor = sdb.rawQuery(sql, new String[]{personid});
        String[] str = new String[cursor.getCount()];
        int i = 0;
        while (cursor.moveToNext()) {
            String name = cursor.getString(0);
            str[i] = name;
            i++;
        }
        cursor.close();
        return str;
    }

    public int findcount(String personid) {
        SQLiteDatabase sdb = dbHelper.getReadableDatabase();

        String sql = "select name from Book,person where Book.bookid=person.bookid and personid=?";
        Cursor cursor = sdb.rawQuery(sql, new String[]{personid});
        String[] str = new String[cursor.getCount()];
        int i = 0;
        while (cursor.moveToNext()) {
            i++;
        }
        cursor.close();
        return i;
    }

    public int findbookid(String name) {
        SQLiteDatabase sdb = dbHelper.getReadableDatabase();
        String sql = "select bookid from Book where name=?";
        Cursor cursor = sdb.rawQuery(sql, new String[]{name});
        int bookid = 0;
        if (cursor.moveToFirst()) {
            cursor.move(0);
            bookid = cursor.getInt(0);
        }
        cursor.close();
        return bookid;
    }

    public String findpersonid(String name) {
        SQLiteDatabase sdb = dbHelper.getReadableDatabase();
        String sql = "select personid from person,Book where person.bookid=Book.bookid and name=?";
        Cursor cursor = sdb.rawQuery(sql, new String[]{name});
        String personid = null;
        if (cursor.moveToNext()) {
            cursor.move(0);
            personid = cursor.getString(0);
        }
        cursor.close();
        return personid;
    }

    public String findnewday(String name) {
        SQLiteDatabase sdb = dbHelper.getReadableDatabase();
        String sql = "select newday from Book where name=?";
        Cursor cursor = sdb.rawQuery(sql, new String[]{name});
        String newday = null;
        while (cursor.moveToNext()) {
            newday = cursor.getString(0);
        }
        cursor.close();
        return newday;
    }

    public String findnewtime(String name) {
        SQLiteDatabase sdb = dbHelper.getReadableDatabase();
        String sql = "select newtime from Book where name=?";
        Cursor cursor = sdb.rawQuery(sql, new String[]{name});
        String newtime = null;
        while (cursor.moveToNext()) {
            newtime = cursor.getString(0);
        }
        cursor.close();
        return newtime;
    }

    public void debook(int bookid) {
        SQLiteDatabase sdb = dbHelper.getReadableDatabase();
        String sql = "delete from Book where bookid = ?";
        Object obj[] = {bookid};
        sdb.execSQL(sql, obj);
    }

    public void deperson(int bookid) {
        SQLiteDatabase sdb = dbHelper.getReadableDatabase();
        String sql = "delete from person where bookid = ?";
        Object obj[] = {bookid};
        sdb.execSQL(sql, obj);
    }

    public boolean addbook(String name, String newday, String newtime) {
        SQLiteDatabase sdb = dbHelper.getReadableDatabase();
        String sql = "insert into Book(name,newday,newtime) values(?,?,?)";
        Object obj[] = {name, newday, newtime};
        sdb.execSQL(sql, obj);
        return true;
    }

    public boolean addperson(int bookid, String personid) {
        SQLiteDatabase sdb = dbHelper.getReadableDatabase();
        String sql = "insert into person(bookid,personid) values(?,?)";
        Object obj[] = {bookid, personid};
        sdb.execSQL(sql, obj);
        return true;
    }

    public boolean changebook(String str1, int bookid) {
        SQLiteDatabase sdb = dbHelper.getReadableDatabase();
        String sql = "update Book set name = ? where bookid = ?";
        Object obj[] = {str1, bookid};
        sdb.execSQL(sql, obj);
        return true;
    }

    public boolean changenewday(String str1, int bookid) {
        SQLiteDatabase sdb = dbHelper.getReadableDatabase();
        String sql = "update Book set newday = ? where bookid = ?";
        Object obj[] = {str1, bookid};
        sdb.execSQL(sql, obj);
        return true;
    }

    public boolean changenewtime(String str1, int bookid) {
        SQLiteDatabase sdb = dbHelper.getReadableDatabase();
        String sql = "update Book set newtime = ? where bookid = ?";
        Object obj[] = {str1, bookid};
        sdb.execSQL(sql, obj);
        return true;
    }

}