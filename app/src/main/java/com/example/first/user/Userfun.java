package com.example.first.user;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.first.User;

public class Userfun {
    private MyDatabaseHelper dbHelper;

    public Userfun(Context context) {
        dbHelper = new MyDatabaseHelper(context);
    }

    public boolean login(String username, String password) {
        SQLiteDatabase sdb = dbHelper.getReadableDatabase();
        String sql = "select * from user where username=? and password=?";
        Cursor cursor = sdb.rawQuery(sql, new String[]{username, password});
        if (cursor.moveToFirst() == true) {
            cursor.close();
            return true;
        }
        return false;
    }

    public boolean find(String username) {
        SQLiteDatabase sdb = dbHelper.getReadableDatabase();
        String sql = "select username from user where username=?";
        Cursor cursor = sdb.rawQuery(sql, new String[]{username});
        if (cursor.moveToFirst() == true) {
            cursor.close();
            return false;
        }
        return true;
    }

    public boolean register(User user) {
        SQLiteDatabase sdb = dbHelper.getReadableDatabase();
        String sql = "insert into user(username,password) values(?,?)";
        Object obj[] = {user.getUsername(), user.getPassword()};
        sdb.execSQL(sql, obj);
        return true;
    }
}